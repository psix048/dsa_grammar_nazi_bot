# dsa_grammar_nazi_bot

first, create new bot with @BotFather and get his api_token

# install on debian11 with python3.9

dpkg-reconfigure locales

reboot

apt install python3-pip git

cd /root

git clone https://gitlab.com/psix048/dsa_grammar_nazi_bot.git

cd /root/dsa_grammar_nazi_bot

python3.9 -m venv bot_venv
echo "aiogram==3.0.0b7" > requirements.txt
echo "python-dotenv==0.21.1" > requirements.txt
echo "requests" > requirements.txt
source bot_venv/bin/activate
pip install -r requirements.txt
deactivate

# autorun (put api_token here)

nano /etc/systemd/system/dsa_bot.service
```
[Unit]
Description = dsa grammar telegram bot
After = network.target

[Service]
Type = simple
Environment="API_TOKEN=xxxxxxxxxxxxxxxxxxx"
ExecStart = /usr/bin/python3.9 /root/dsa_grammar_nazi_bot/main.py
User = root
Group = root
Restart = on-failure
SyslogIdentifier = dsa_bot
RestartSec = 5
TimeoutStartSec = infinity

[Install]
WantedBy = multi-user.target
```
systemctl daemon-reload

systemctl enable dsa_bot

# quick update source
##   prepare
echo "service dsa_bot stop && git -C /root/dsa_grammar_nazi_bot/ checkout -f && service dsa_bot start" > /usr/local/bin/reload_bot && chmod 700 /usr/local/bin/reload_bot

##   run
reload_bot
