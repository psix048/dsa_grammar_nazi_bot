import requests
import asyncio
import logging
from aiogram import Bot, Dispatcher, types
from aiogram.filters.command import Command
from aiogram.filters import Text
from os import getenv
from sys import exit

from aiogram.utils.keyboard import InlineKeyboardBuilder

logging.basicConfig(level=logging.INFO)
token = getenv("API_TOKEN")
if not token:
    exit("Error: no token provided")

bot = Bot(token=token, parse_mode="MarkdownV2")
dp = Dispatcher()
url = "https://speller.yandex.net/services/spellservice.json/checkText"


# этот метод больше не работает. да и раньше тоже, вроде бот требует повышенных прав для сообщений
# @dp.message_handler(content_types=['new_chat_members'])
# async def new_chat_members(message):
# msg = "_Брутальный *ГраммарНаци* вошёл в чат_\r\n\r\n"
# msg += "Теперь Я буду следить за вами, неграмотные кожаные ублюдки\.\r\n"
# msg += "Я пока туповат \(v\.1\.1\) и не всё замечаю, но уже умею исправлять вашу писанину\. Чтож, начнём, пожалуй\.\.\.\r\n\r\n"
# msg += "Добро пожаловать в ад\!"
# await message.answer(msg)


@dp.message(Command('start'))
async def send_welcome(message: types.Message):
    await message.reply("Я - ГраммарНаци! Пиши сюда, Я тебя проверю.")


@dp.message()
async def echo(message: types.Message):
    text_message = fix_message(message.text)
    if text_message is not None:
        builder = InlineKeyboardBuilder()
        builder.add(types.InlineKeyboardButton(
            text="Я исправил!",
            callback_data="Я исправил!")
        )
        await message.reply(text_message, reply_markup=builder.as_markup())


@dp.callback_query(Text("Я исправил!"))
async def send_random_value(callback: types.CallbackQuery):
    text_message = fix_message(callback.message.reply_to_message.text)
    if not text_message:
        await callback.answer("МАЛАДЕЦ!")
        await callback.message.delete()
        return

    await callback.answer("Не исправил: \r\n" + text_message)


def fix_message(text):
    jsondata = {"text": text, "lang": "ru", "options": 518}
    fixes = requests.post(url, data=jsondata).json()
    if not fixes: return
    fixed = text
    for fix in fixes:
        corr = "~~~" + fix["word"] + "~~~ *" + fix["s"][0] + "*"
        fixed = fixed.replace(fix["word"], corr)
    return fixed


# Запуск процесса поллинга новых апдейтов
async def main():
    await dp.start_polling(bot)


if __name__ == '__main__':
    asyncio.run(main())
